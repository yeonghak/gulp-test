//引入gulp
const { src, dest, watch, series, parallel } = require('gulp');

//引入插件包
const cleanCSS = require('gulp-clean-css'); //css文件压缩
const prettify = require('gulp-prettify'); //html文件格式化
const prettifyOptions = require('./prettify'); //html文件格式化配置文件
const imagemin = require('gulp-imagemin'); //图片压缩
const minify = require('gulp-minify'); //js文件压缩
const concat = require('gulp-concat'); //文件合并
const autoprefixer = require('gulp-autoprefixer'); //css自动补全前缀
const browserSync = require('browser-sync').create(); //自动刷新浏览器

//自动刷新浏览器
const live = (done) => {
    browserSync.init({
        server: {
            baseDir: './dist' //编译后项目目录
        },
        watch: true,
        port: 8081,
        open: true,
        notify: true
    });
    done();
};

//html文件处理
const htmls = () => {
    const files = './src/app/**/*.html'; //指定要处理的文件
    return src(files)
        .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
        .pipe(prettify(prettifyOptions)) //格式化html代码
        .pipe(dest('./dist')); //输出文件
};

//css文件处理
const styles = () => {
    const files = './src/css/**/*.css'; //指定要处理的文件
    return src(files)
        .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
        .pipe(autoprefixer()) //自动补全前缀
        .pipe(concat('test.css')) //合并css文件
        .pipe(cleanCSS({ //压缩文件
            compatibility: 'ie8',
            format: 'beautify'
        }))
        .pipe(dest('./dist/css')) //输出文件
        .pipe(concat('test.min.css')) //合并css文件
        .pipe(cleanCSS({ //压缩文件
            compatibility: 'ie8'
        }))
        .pipe(dest('./dist/css')); //输出文件
};

//js文件处理
const scripts = () => {
    const files = './src/js/**/*.js'; //指定要处理的文件
    return src(files)
        .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
        .pipe(concat('test.js')) //合并js文件
        .pipe(minify({ //压缩js文件
            ext: {
                min: '.min.js'
            },
            noSource: false
        }))
        .pipe(dest('./dist/js')); //输出文件
};

//图片文件处理
const images = () => {
    const files = './src/images/**/*.*'; //指定要处理的文件
    return src(files)
        .pipe(imagemin()) //压缩图片
        .pipe(dest('./dist/images')); //输出文件
};

//监听事件
const watcher = (done) => {
    watch('./src/app/**/*.html', parallel(htmls)).on('change', browserSync.reload);
    watch('./src/css/**/*.css', parallel(styles)).on('change', browserSync.reload);
    watch('./src/js/**/*.js', parallel(scripts)).on('change', browserSync.reload);
    watch('./src/images/**/*.*', parallel(images)).on('change', browserSync.reload);
    done();
};

//任务声明
const dev = series(parallel(styles, scripts, htmls, images), live, watcher);

//默认任务指定
exports.default = dev;